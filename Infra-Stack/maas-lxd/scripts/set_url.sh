#!/bin/bash

CONTAINER=$1

IPADDRESS=$(lxc info $CONTAINER | grep -Po '((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}' -m1)

echo "Setting region and rack to http://$IPADDRESS:5240/MAAS"
lxc exec $CONTAINER -- sudo maas-region local_config_set --maas-url="http://$IPADDRESS:5240/MAAS"
lxc exec $CONTAINER -- sudo maas-rack config --region-url="http://$IPADDRESS:5240/MAAS"
