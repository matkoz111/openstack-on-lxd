#!/bin/bash

CONTAINER=$1
[[ -n "$1" ]] || CONTAINER=maas-lxd 

lxc version | grep "Server version" >/dev/null 2>&1

# lxc version > 3 has dififerent output
if [ $? -eq 0 ]; then
    # Find Major/Minor LXC version 3
    VER=$(lxc version | grep Server | awk -F: '{print $2}'i | sed 's/\s\+//g')
    MAJOR=$(echo $VER | awk -F. '{print $1}')
    MINOR=$(echo $VER | awk -F. '{print $2}')
else
    # Find Major/Minor < LXC version 3
    MAJOR=$(lxc version | awk -F. '{print $1}')
    MINOR=$(lxc version | awk -F. '{print $2}')
fi


# Create container
echo "Enter the profile(maas-profile or maas-profile-basic) name or leave blank to use a standard maas profile:"
read maasprofile

until [[ $clu = 'y' || $clu = 'n' ]]; do
    read -p "Are you using cluster?(y/n)" clu
done

echo "Creating container $CONTAINER from profile $maasprofile"
[[ "$maasprofile" == "" ]] && maasprofile="maas-profile"
if [[ "$clu" == "y" ]]; then echo 'Creating network for maas, if on one noede of cluster you have to finish creation on all nodes...' ; lxc network create maasbr0 --target $HOSTNAME; elif [[ "$clu" == "n" ]]; then lxc network create maasbr0; fi 
case $maasprofile in
maas-profile) lxc profile create $maasprofile 2>/dev/null ; lxc profile edit $maasprofile <<<$(cat $maasprofile | sed -e 's/IP_ADDR_10/10.10.10.101/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.98/g') 
if [ $MAJOR -eq 5 ]; then
    sed 's/lxc.aa_profile/lxc.apparmor.profile/g' -i maas-profile
fi
;;
maas-profile-basic) lxc profile create $maasprofile 2>/dev/null ; lxc profile edit $maasprofile <<<$(cat $maasprofile) ;;
*)
    echo "Specify correct maas profile file"
    exit 1
    ;;
esac

[[ "$maasprofile" == "maas-profile-basic" ]] && (lxc profile create maas-db 2>/dev/null ; lxc profile edit maas-db <<<$(cat db-profile.basic)) || (lxc profile create maas-db 2>/dev/null && lxc profile edit maas-db <<<$(cat db-profile | sed -e 's/IP_ADDR_10/10.10.10.121/g'))

[[ $(lxc storage ls | grep local-lvm) == "" ]] && echo 'Create local-lvm lvm pool needed by 2 instances as described here https://linuxcontainers.org/lxd/docs/master/howto/storage_pools/#create-a-storage-pool-in-a-cluster'

lxc init images:ubuntu/20.04/cloud infradb -p maas-db
echo "Creating MAAS..."
if [ $MAJOR -eq 5 ]; then
    # v3 expects pool name, use default
    lxc launch images:ubuntu/20.04/cloud $CONTAINER -p $maasprofile 
elif [ $MAJOR -ne 5 && $MINOR -ne 0 ]; then
    lxc launch images:ubuntu/20.04/cloud $CONTAINER -p $maasprofile
else
    lxc profile create root-device 2>/dev/null
    lxc profile edit root-device <root-device
    lxc launch images:ubuntu/18.04/cloud $CONTAINER -p $maasprofile -p root-device
fi

echo "Sleeping to wait for IP"
sleep 10

# Setup LXD forward for pxe requests
IPADDRESS=$(lxc info $CONTAINER | grep ' br0' -A 12 | grep -Po '((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}')

if lxc network set maasbr0 raw.dnsmasq dhcp-boot=pxelinux.0,$CONTAINER,$IPADDRESS; then
    echo "pxe redirect setup for IP $IPADDRESS"

    if snap services | grep -e '^lxd.daemon\s\+enabled\s\+active' >/dev/null 2>&1; then
        # Snap-based LXD.  This appears to be safe.  (Thanks csanders)
        sudo systemctl reload snap.lxd.daemon
    fi
else
    echo "No maasbr0 found, unable to setup pxe redirect"
    echo "Manual config of raw.dnsmasq for pxe redirect needs to be done"
fi

echo "MAAS will become available at: http://$IPADDRESS:5240/MAAS with user/password admin/admin"
