---
# Installs MAAS

- name: Install MAAS - Snap
  community.general.snap:
    name: maas
    channel: '{{ maas_version }}/{{ maas_snap_channel }}'
    state: "{{ 'present' if maas_state|lower == 'install' else maas_state }}"
  register: maas_region_new_installation
  until: "maas_region_new_installation is not failed"
  retries: 3
  when: maas_installation_type|lower == 'snap'

- name: Add MAAS apt Respository
  ansible.builtin.apt_repository:
    repo: "ppa:maas/{{ maas_version }}"
  when: maas_installation_type|lower == 'deb'

- name: Install MAAS Region Controller - Deb
  ansible.builtin.apt:
    name: "maas-region-api"
    state: "{{ maas_deb_state|default('present') }}"
    update_cache: true
  register: maas_region_new_installation
  when: (maas_installation_type|lower == 'deb')

- name: Update regiond.conf
  ansible.builtin.template:
    src: regiond.conf.j2
    dest: /etc/maas/regiond.conf
    mode: 0644
    owner: maas
    group: maas
  when: maas_installation_type|lower == 'deb'

- name: "Pause as maas cannot be initialized  too fast"
  ansible.builtin.pause:
    seconds: 30

- name: Making sure that the conf file is present
  wait_for:
    path:  /var/snap/maas/current
    delay: 10
    timeout: 30
    state: present
    msg: "Specified CONF FILE is still not  present"

- name: Update regiond.conf - Snap
  ansible.builtin.template:
    src: regiond.conf.j2
    dest: /var/snap/maas/current/regiond.conf
    mode: 0640
    owner: root
    group: root
  when: maas_installation_type|lower == 'snap'

- name: Initialise MAAS Controller - Snap
  ansible.builtin.shell: if [ -n "$(maas status | grep 'RUNNING')" ] ; then echo 'maas already initialised' ; else maas init {{ 'region+rack' if 'maas_rack_controller' in group_names else 'region' }} --maas-url={{ maas_url_notls }} --database-uri {{ maas_postgres_uri }} ; fi 
  when: maas_installation_type|lower == 'snap' and maas_region_new_installation is defined
  register: initresult
  until: "initresult is not failed"
  retries: 4

- name: Migrate MAAS database
  ansible.builtin.shell: "{{ 'maas' if maas_installation_type|lower == 'snap' else 'maas-region' }} migrate"
  run_once: true
  when: "'Starting service' in initresult.stdout"
  register: migrateresult
  until: "migrateresult is not failed"
  retries: 4

# MAAS region controller only needs to be initialized in this case if rbac or candid are in use, otherwise the reigond.conf write handles init
- name: Initialise MAAS Controller - Deb
  ansible.builtin.expect:
    command: "maas init --rbac-url={{ maas_rbac_url|default('')|quote }} --candid-agent-file={{ maas_candid_auth_file|default('')|quote }} --admin-ssh-import={{ admin_id }}"
    responses:
      "(?i)Username: ": "{{ admin_username }}"
      "(?i)Password: ": "{{ admin_password }}"
      "(?i)Again: ": "{{ admin_password }}"
      "(?i)Email: ": "{{ admin_email }}"
  when: maas_installation_type|lower == 'deb' and maas_region_new_installation is defined

- name: Starting MAAS region service
  ansible.builtin.systemd:
    name: maas-regiond.service
    state: started
    no_block: no
  when: maas_installation_type|lower == 'deb'

- name: Define MAAS URL
  ansible.builtin.command: maas config | grep maas_url | cut -d "=" -f2
  register: maas_url
  when: not maas_url

- name: Add an administrator to MAAS
  ansible.builtin.command: maas createadmin \
   --username={{ admin_username }} --password={{ admin_password }} \
   --email={{ admin_email }} --ssh-import={{ admin_id }}
  when: not maas_region_new_installation or maas_installation_type|lower == 'snap' and 'Starting service' in initresult.stdout
  run_once: true

- name: Enable TLS
  ansible.builtin.include_role:
    name: common
    tasks_from: TLS
  # TLS is available only in MAAS 3.2 or higher
  when: ('3.' in maas_version or maas_version == 'latest') and enable_tls and '.0' not in maas_version and '.1' not in maas_version

- name: Wait For MAAS To Create Secret File
  ansible.builtin.wait_for:
    path: "{{ maas_secret_file }}"
    state: present

- name: Read MAAS Secret For Rack Controllers
  ansible.builtin.command: cat "{{ maas_secret_file }}"
  register: maas_rack_secret_tmp

- name: Save MAAS Secret
  set_fact:
    maas_rack_secret: "{{ maas_rack_secret_tmp.stdout }}"
    cacheable: yes
  run_once: true
  delegate_to: "{{ item }}"
  delegate_facts: true
  loop: "{{ groups['maas_rack_controller'] }}"
