#!/bin/bash

[[ -n "$(lxc ls | grep maas-ha)" ]] && lxc stop -f maas-ha-1 maas-ha-2 maas-ha-3 maas-ha-db-1 maas-ha-db-2 maas-proxy-1 maas-vault-1 && lxc delete maas-ha-1 maas-ha-2 maas-ha-3 maas-ha-db-1 maas-ha-db-2 maas-proxy-1 maas-vault-1

lxc launch images:ubuntu/jammy/cloud  maas-ha-1 -p container-vlan10-maas --target infra1 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.101/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.101/g;s/IP_ADDR_BR0/10.184.152.103/g')"
lxc launch images:ubuntu/jammy/cloud  maas-ha-2 -p container-vlan10-maas --target infra2 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.102/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.102/g;s/IP_ADDR_BR0/10.184.152.103/g')"
lxc launch images:ubuntu/jammy/cloud  maas-ha-3 -p container-vlan10-maas --target infra3 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.103/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.103/g;s/IP_ADDR_BR0/10.184.152.103/g')"
sleep 5
lxc launch images:ubuntu/jammy/cloud  maas-ha-db-1 -p container-vlan10 --target infra1 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container.yaml | sed -e 's/IP_ADDR_10/10.10.10.105/g;s/IP_GATEWAY/10.10.10.1/g')"
lxc launch images:ubuntu/jammy/cloud  maas-ha-db-2 -p container-vlan10 --target infra2 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container.yaml | sed -e 's/IP_ADDR_10/10.10.10.106/g;s/IP_GATEWAY/10.10.10.1/g')" 
sleep 5
lxc launch images:ubuntu/jammy/cloud  maas-proxy-1 -p container-vlan10 --target infra1 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container.yaml | sed -e 's/IP_ADDR_10/10.10.10.104/g;s/IP_GATEWAY/10.10.10.1/g')"
lxc launch images:ubuntu/jammy/cloud  maas-vault-1 -p container-vlan10 --target infra2 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container.yaml | sed -e 's/IP_ADDR_10/10.10.10.107/g;s/IP_GATEWAY/10.10.10.1/g')"
