#!/bin/bash

lxc network create maasbr0 --target infra1
lxc network create maasbr0 --target infra2
lxc network create maasbr0 --target infra3
lxc network create maasbr0 -c ipv4.dhcp=false ipv6.dhcp=false ipv6.address=none