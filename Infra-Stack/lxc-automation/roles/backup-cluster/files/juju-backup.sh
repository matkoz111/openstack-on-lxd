#!/bin/bash

echo -e "Starting Mysql backup(saving to location on the cluster leader which is \n$(juju status|grep 'mysql-innodb-cluster.*\*' ))"
juju run --wait 120s mysql-innodb-cluster/leader mysqldump basedir=/opt

echo -e "Starting ETCD backup(saving to location on the cluster leader which is \n$(juju status|grep 'etcd.*\*' ))"

echo -e "Starting juju backup which will be saved to /root and then relocated to /opt/backups"
juju switch controller ; juju create-backup
[[ -d /opt/backups ]] || mkdir -p /opt/backups
mv juju-backup* /opt/backups/