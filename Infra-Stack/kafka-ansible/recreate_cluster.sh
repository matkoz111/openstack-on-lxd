#!/bin/bash

[[ -n "$(lxc ls | grep kafka-ha)" ]] && lxc stop -f kafka-ha-1 kafka-ha-2 kafka-ha-3 && lxc delete kafka-ha-1 kafka-ha-2 kafka-ha-3 

lxc launch images:ubuntu/jammy/cloud  kafka-ha-1 -p container-vlan10 --target infra1 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.151/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.151/g;s/IP_ADDR_BR0/10.184.152.151/g')"
lxc launch images:ubuntu/jammy/cloud  kafka-ha-2 -p container-vlan10 --target infra2 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.152/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.152/g;s/IP_ADDR_BR0/10.184.152.152/g')"
lxc launch images:ubuntu/jammy/cloud  kafka-ha-3 -p container-vlan10 --target infra3 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.153/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.153/g;s/IP_ADDR_BR0/10.184.152.153/g')"
