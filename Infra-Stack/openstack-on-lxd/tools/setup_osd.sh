#!/bin/bash

[[ -n "$(lxc ls | grep openstack-osd)" ]] && lxc stop -f openstack-osd-1 openstack-osd-2 && lxc delete openstack-osd-1 openstack-osd-2

lxc launch images:ubuntu/jammy/cloud  openstack-osd-1 -p juju-openstack --target infra1 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.171/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.171/g;s/IP_ADDR_BR0/10.184.152.171/g')"
lxc launch images:ubuntu/jammy/cloud  openstack-osd-1 -p juju-openstack --target infra2 -c user.network-config="$(cat /home/mother/cloud-init-scripts/network-config-infra-container-maas.yaml | sed -e 's/IP_ADDR_10/10.10.10.172/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.172/g;s/IP_ADDR_BR0/10.184.152.172/g')"

lxc storage volume create remote ceph1 size=60GB --type block && lxc storage volume create remote ceph2 size=60GB --type block

lxc storage volume attach remote ceph1 openstack-osd-1 && lxc storage volume attach remote ceph2 openstack-osd-2