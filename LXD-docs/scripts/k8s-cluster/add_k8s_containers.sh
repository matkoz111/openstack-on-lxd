#!/bin/bash

remove_services=${remove_services:-true}

if [[ "$remove_services" == true ]]; then
    echo Removing services...
    for i in 1 2 3; do lxc exec k8s-master-ha-$i -- /usr/sbin/remove-juju-services; done
    for i in 1 2 3; do lxc exec etcd-ha-$i -- /usr/sbin/remove-juju-services; done
    for i in 1 2 3; do lxc exec k8s-innodb-$i -- /usr/sbin/remove-juju-services; done
fi

lxc exec k8s-lb-1 -- /usr/sbin/remove-juju-services
for i in 1 2 3; do ssh-keygen -f "/root/.ssh/known_hosts" -R  "10.10.10.14$i"; done
for i in 1 2 3; do ssh-keygen -f "/root/.ssh/known_hosts" -R  "10.10.10.13$i"; done
for i in 6 7 8; do ssh-keygen -f "/root/.ssh/known_hosts" -R  "10.10.10.12$i"; done
ssh-keygen -f "/root/.ssh/known_hosts" -R  "10.10.10.135"

echo Echoing...
for i in 1 2 3; do ssh mother@10.10.10.14$i -i ~/.local/share/juju/ssh/juju_id_rsa -- echo y; done
for i in 1 2 3; do ssh mother@10.10.10.13$i -i ~/.local/share/juju/ssh/juju_id_rsa -- echo y; done
for i in 6 7 8; do ssh mother@10.10.10.12$i -i ~/.local/share/juju/ssh/juju_id_rsa -- echo y; done
ssh mother@10.10.10.135 -i ~/.local/share/juju/ssh/juju_id_rsa -- echo y

echo Adding...
for i in 1 2 3; do juju add-machine ssh:mother@10.10.10.14$i; done
for i in 1 2 3; do juju add-machine ssh:mother@10.10.10.13$i; done
for i in 6 7 8; do juju add-machine ssh:mother@10.10.10.12$i; done
juju add-machine ssh:mother@10.10.10.135


###################################################################
#commands which need to be executed prior to
###################################################################
<<'END'
j add-model k8s maas --config "project=k8s"

j deploy kubernetes-worker --channel 1.25/beta --constraints "tags=worker" --config channel=1.25/beta
j deploy vault --channel latest/edge --constraints "tags=vault" --config "auto-generate-root-ca-cert=true"
j deploy kubernetes-control-plane -n 3 --channel 1.25/beta --to 0,1,2 --config channel=1.25/beta
j deploy etcd --channel 1.25/beta -n 3 --to 3,4,5 --config channel=3.4/stable
j deploy mysql-innodb-cluster --channel 8.0/stable -n 3 --to 6,7,8 --config "enable-binlogs=true" --config "innodb-buffer-pool-size=256M" --config max-connections=2000 --config wait-timeout=3600
OR
j deploy mysql --channel 8.0/stable -n 3 --to 6,7,8
THEN
j deploy kubeapi-load-balancer --channel 1.25/beta --to 9
j deploy containerd --channel 1.25/beta
j deploy --channel 8.0/stable mysql-router vault-mysql-router
j deploy calico --channel 1.25/beta --config "disable-vxlan-tx-checksumming=true" --config "ignore-loose-rpf=true" --config "vxlan=Never"
juju export-bundle --filename current.yaml
OR JUST
j deploy ./current.yaml --map-machines=existing
END
###################################################################
