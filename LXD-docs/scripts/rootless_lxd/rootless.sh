#!/bin/bash

sudo curl -fsSL -o /usr/local/bin/generate-lxc-perms https://gist.githubusercontent.com/3XX0/ef77403389ffa1ca85d4625878706c7d/raw/4f0d2c02d82236f74cf668c42ee72ab06158d1d2/generate-lxc-perms.sh
 sudo chmod ugo+rx /usr/local/bin/generate-lxc-perms

sudo curl -fsSL -o /usr/local/bin/generate-lxc-config https://gist.githubusercontent.com/3XX0/b3e2bd829d43104cd120f4258c4eeca9/raw/890dc720e1c3ad418f96ba8529eae028f01cc994/generate-lxc-config.sh
sudo chmod ugo+rx /usr/local/bin/generate-lxc-config


sudo tee /usr/share/lxc/config/common.conf.d/nvidia.conf <<< 'lxc.hook.mount = /usr/share/lxc/hooks/nvidia'
sudo chmod ugo+r /usr/share/lxc/config/common.conf.d/nvidia.conf

sudo generate-lxc-perms
generate-lxc-config