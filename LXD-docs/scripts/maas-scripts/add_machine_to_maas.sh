#!/bin/bash

#functions
usage() {
  echo "Usage: $0 [-n <machine name>] [-m <maas ip addr>] [-h]"
  exit 1
}

reg_in_maas() {
    wget "http://$2:5240/MAAS/maas-run-scripts" && chmod 0755 maas-run-scripts

    ./maas-run-scripts register-machine --hostname "$1" "http://$2:5240/MAAS" "$3"

    ./maas-run-scripts report-results --config "$1-creds.yaml"
}

function main() {
  read -s -p "Enter MAAS token: " secret_variable

  reg_in_maas "$@" "$secret_variable"
}

while getopts "n:m:h" opt; do
  case $opt in
    n)
      machine=$OPTARG
      ;;
    m)
      maas_addr=$OPTARG
      ;;
    h)
      usage
      ;;
    *)
      usage
      ;;
  esac
done

if [ -z "$machine" ] || [ -z "$maas_addr" ]; then
  echo "Both -n and -m parameters are required"
  usage
fi

#main

main "$machine" "$maas_addr"