```
root@hs-worker-2:~# cat /proc/cmdline 
BOOT_IMAGE=/boot/vmlinuz-5.4.0-109-generic root=UUID=a45363a1-3529-4b9c-9dd6-2dd33c683c3f ro intel_iommu=on iommu=pt rd.driver.blacklist=nouveau nouveau.modeset=0 vfio-pci.ids=10de:0df8 noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off mitigations=off tsx=on tsx_async_abort=off

root@hs-worker-2:~# dmesg  | grep -i iommu | grep -i group | head -n10
[    1.233773] pci 0000:00:00.0: Adding to iommu group 0
[    1.233854] pci 0000:00:01.0: Adding to iommu group 1
[    1.233926] pci 0000:00:01.1: Adding to iommu group 2
[    1.234002] pci 0000:00:02.0: Adding to iommu group 3
[    1.234078] pci 0000:00:03.0: Adding to iommu group 4
[    1.234148] pci 0000:00:05.0: Adding to iommu group 5
[    1.234219] pci 0000:00:05.2: Adding to iommu group 6
[    1.234300] pci 0000:00:05.4: Adding to iommu group 7
[    1.234372] pci 0000:00:11.0: Adding to iommu group 8
[    1.234459] pci 0000:00:16.0: Adding to iommu group 9

root@hs-worker-2:~# for d in /sys/kernel/iommu_groups/*/devices/*; do n=${d#*/iommu_groups/*}; n=${n%%/*}; printf 'IOMMU Group %s ' "$n"; lspci -nns "${d##*/}"; done | grep NVIDIA;

cat <<EOF >/tmp/cloudinit
#cloud-config
package_update: false
package_upgrade: false
disable_ec2_metadata: true
locale: en_US.UTF-8
ssh_pwauth: True
chpasswd:
  list: |
     root:ubuntu
  expire: False
EOF

n=$(date +%s)
lxc init ubuntu:18.04 ubuntu-$n --vm
cat /tmp/cloudinit | lxc config set ubuntu-$n user.user-data -
lxc config device add ubuntu-$n config disk source=cloud-init:config
lxc config device add ubuntu-$n my-gpu gpu gputype=physical pci=04:00.0
lxc start ubuntu-$n

root@ubuntu-1650543920:~# lspci  | grep VGA
04:00.0 VGA compatible controller: Red Hat, Inc. Virtio GPU (rev 01)
06:00.0 VGA compatible controller: NVIDIA Corporation GF108GL [Quadro 600] (rev a1)

root@ubuntu-1650543920:~# dmesg | grep nvidia
[    1.816669] nvidia: loading out-of-tree module taints kernel.
[    1.818213] nvidia: module license 'NVIDIA' taints kernel.
[    1.873509] nvidia-nvlink: Nvlink Core is being initialized, major device number 242
[    1.890630] nvidia 0000:06:00.0: vgaarb: changed VGA decodes: olddecodes=io+mem,decodes=none:owns=none
[    1.918519] nvidia-modeset: Loading NVIDIA Kernel Mode Setting Driver for UNIX platforms  390.147  Mon Dec 13 13:51:26 UTC 2021
[    1.923027] [drm] [nvidia-drm] [GPU ID 0x00000600] Loading driver
[    1.924629] [drm] Initialized nvidia-drm 0.0.0 20160202 for 0000:06:00.0 on minor 0
[    4.127228] nvidia-uvm: Loaded the UVM driver in 8 mode, major device number 241

root@ubuntu-1650543920:~# apt install nvidia-driver-390
```
