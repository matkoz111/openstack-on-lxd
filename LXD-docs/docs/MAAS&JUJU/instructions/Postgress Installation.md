# Installing PostgreSQL cluster

## Setting up containers for PostgreSQL
```
lxc init images:ubuntu/20.04/cloud infradb1 -p infra --target infra1
vim cloud-init.infra.yaml
cat cloud-init-infra.1.yaml | sed -e 's/IP_ADDR_10/10.10.10.101/g;s/IP_GATEWAY/10.10.10.1/g' | lxc config set infradb1 user.network-config -
lxc start infradb1
```

## Refreshing packages and installing snapd
```
lxc exec infradb1 bash
apt-get update
apt-get upgrade
apt-get install snapd
```

## Installing PostgreSQL
```
apt-get install gnupg
apt-get install wget
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/postgresql-pgdg.list
sudo apt-get update
sudo apt install postgresql-14
```

## Preparing the shared folder for the data
```
mkdir -p /shared/postgresql
chown -R postgres:postgres /shared/postgresql
```

## Configuration of the new custom cluster
```
service postgresql stop
vim /etc/postgresql-common/createcluster.conf
/usr/lib/postgresql/14/bin/pg_ctl init -D /shared/postgresql
/usr/lib/postgresql/14/bin/pg_ctl start -D /shared/postgresql -l /shared/postgresql/postgresql-14-main.log

data_directory = '/shared/postgresql/%v/%c'
waldir = '/shared/postgresql/wal/%v/%c/pg_wal'

service postgresql start
```

## Creating the custom database cluster
```
sudo su - postgres -D /shared/postgresql -l /shared/postgresql/postgresql-14-main.log start
pg_createcluster -start 14 maas
pg_lsclusters
service postgresql start
service postgresql status
```

## Dropping the default main cluster
```
pg_lsclusters
systemctl stop postgresql@14-main
pg_dropcluster -stop 14 main
pg_lsclusters
```

## Configuring networking
```
vim /etc/netplan/10-lxc.yaml 
netplan apply /etc/netplan/10-lxc.yaml
ip a
```

### Adding md5 authentication to the maas database
```
vim /etc/postgresql/14/maas/pg_hba.conf 
# MAAS database
host    maas            maas            0/0                     md5
```

### Configuring listening address and port
```
vim /etc/postgresql/14/maas/postgresql.conf

systemctl stop postgresql@14-maas
systemctl start postgresql@14-maas
service postgresql stop
service postgresql start
```

## Creating the database for MAAS and the dbuser
```
export MAAS_DBUSER=maas
export MAAS_DBPASS=infra123
export MAAS_DBNAME=maas
env
sudo -u postgres psql -c "CREATE USER \"$MAAS_DBUSER\" WITH ENCRYPTED PASSWORD '$MAAS_DBPASS'"
sudo -i -u postgres createdb -O "$MAAS_DBUSER" "$MAAS_DBNAME"
```

## Verifying connectivity to the database
```
pg_lsclusters
pg_isready -d maas -h localhost -p 5432 -U maas 
ping 10.10.10.111
ping 10.10.10.111:5432
ping localhost:5432

pg_isready -d maas -h localhost -p 5432 -U maas 
ping localhost:5432

nc -vz 10.10.10.111 5432
nc -vz 10.10.10.111 5433
nc -vz localhost 5432
nc -vz localhost 5433

nc -vz 10.10.10.111 5432
nc -vz localhost 5432

apt install net-tools
netstat -lntp
vim /etc/postgresql/14/custom/postgresql.conf 
service postgresql stop
service postgresql start
netstat -lntp

systemctl stop keepalived
systemctl enable --now keepalived
journalctl -flu keepalived
```
## Repair cluster
```
sudo chown postgres:postgres /shared/postgresql/global/pg_internal.init
sudo rm -rf /shared/postgresql/global/pg_internal.init
sudo rm -rf /shared/postgresql/pg_logical/replorigin_checkpoint
sudo -i -u postgres 
/usr/lib/postgresql/14/bin/pg_ctl restart -D /shared/postgresql
```