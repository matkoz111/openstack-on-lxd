# Setting up Juju infrastructure

## Installing Juju
```
```

## Adding mas-lub cloud
```
juju add-cloud --local
maas-lub
QVkV9rnXC7yNqtPRZr:WREPU6XKHFVB3ghkFA:ZALUrJPmWtuFpTYURVVzA4ELjmrkeTd3

juju add-credential maas-lub
juju bootstrap --credential maas-lub-credential maas-lub/default juju1 --to juju-controller1.maas --config bootstrap-timeout=3600 --bootstrap-constraints mem=1.5G
 juju bootstrap maas2 juju-controller-maas-1 --config bootstrap-timeout=3600 --bootstrap-constraints "mem=1.5G tags=juju" --debug
```

## Accessing Juju dashboard
```
juju dashboard

https://10.10.99.253:17070/dashboard
admin
bdb48e8d4ea795b953a9b7a89eca4ec2
```

## Preparing a VM for Juju controller
```
lxc init images:ubuntu/20.04/cloud juju-controller-test -p maas --target infra2 --vm 
lxc config set juju-controller-test limits.cpu=2
lxc config device override juju-controller-test root size=20GB
cat /home/mother/cloud-init-maas.1.yaml | sed -e 's/IP_ADDR_10/10.10.10.98/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.98/g' | lxc config set juju-controller-test user.network-config -
lxc config show juju-controller-test --expanded
lxc start juju-controller-test
```

```
wget http://10.10.10.104:5240/MAAS/maas-run-scripts && chmod 755 maas-run-scripts

./maas-run-scripts register-machine --hostname lxd-k8s-1 http://10.10.10.104:5240/MAAS/ Gebpy4fTWpFKfgkEhk:ckcNf3YCd9F3TZeugN:h3X9JNL9vws9GjwLwqmdWk26HyVd82G2
./maas-run-scripts report-results --config lxd-k8s-1-creds.yaml


lxc config trust add - <<EOF
-----BEGIN CERTIFICATE-----
MIIE1TCCAr0CEQDCqFYDlVWFUMIDUJU2vZMvMA0GCSqGSIb3DQEBDQUAMD4xDTAL
BgNVBAoMBE1BQVMxLTArBgNVBAsMJGQzMmY3Y2Q0LWViZjYtNDA5Ny1hOTJkLWRh
MWY3YzZiMGEwNjAeFw0yMjA3MTkyMDU4MDdaFw0zMjA3MTYyMDU4MDdaMBMxETAP
BgNVBAMMCE1BQVMtUm5EMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA
8GHhEguN/wetWMraUzmtqSRSkoSX5fdK/4ZfwIqv+pJeEkq86V0NVxfZXooe1UPD
KCh1BDsssbpy0sZWHXpldslwnYq56fEM42p07akEMbu+2usK0ubmGVv0wg48RS8n
nLdZKQrx5CqJq2XLpgtkMUo4cdmloolHTgoMUYZweK7jlYjY6fhEW3sObmGq+ftX
gcq/hqryphPusanbLXkMspEYj0Xz/o/QQU7rZ6VMT82+oxVhtU9wJeoAfp08mpz5
TXUX+gC+pkCm5fZndlUec3bZzg5ag6XZEWCZtpftvPjwGDurZqEzC1TQEOnp/TIx
aHYVFUcLhAIDoxmv45letxeZ/xklE1Bh1u/f68h9pjDOkr7Vz3mLsP2WALcBDAS0
/RLpUGb4IYOa4VXs+OCxLHQVsEkMJThDp4z8jEkOYDDYI9t73ToqI4dPBB3uP/z6
l1MsAkEb0KOxXG30Sp0sYZlONG0wi2L1mhT762R7l+HsNnJbLn23EPJN52MWChTd
96fT/0bZnlSHr24/Jr2SlCk7sdfAi1EoeK94QZ8PZadHujCyu0vJZynXO8iHgSb6
PhuzC2CzSVJxa+JM8gmyKVvuTZyvUvg+mWKEDbUV6DLcN2yc4ZPPVw59h+tkKyYY
NVyRzpKhi5n6XgemXKYjmVA02DylmPvEt4ViscobIQcCAwEAATANBgkqhkiG9w0B
AQ0FAAOCAgEAlbvQ/M5x/VGGOCci1o+8k4x7Hsmnr6AKo19sPrgBmhDO8J9efC4R
CY8AofSsXaH8kv9pEmyWqTY74AebENlndZjz2qDOzeU8Qpc9mpYyAeKlFNRbpB1t
jdAwZ8VVLTjNarmXsEVmK0Nv61rI+IcqER9oC1aqrj+dA9CKKtCNAUHb57kiMLmp
/cCKSPaM9r4gz69CC7zoaeYBxtEPbdzozRUdeiUMmXLA9KB+hbThPQIEuyJNQRqs
MT0eoIDoc71dnzyEo+YAbWLf+O6qVcayxozafbkAHnAtwd1lxm2LIe5CUyc+WmIs
Plb7BVn1NdVwLzKEHKxfvkgZgfWVQDuJi3K+qqKGYTZErnGlY4/3YjA4G/zq3dWS
29+d3goErtZd7/3IuywZM2Izi4mnJ4DJwac4AXkVQWeDbYwVTa5tStF98iznMolV
tRwmCBYBvTmsMmukufkfD3NWc9EUBUQ89AblCUGEpNHGsR95Pp07jpNorTW9TINl
V3j3dq8FCdV0csh+lbCMM4pk5CuTPxoH4HKvru2gGTI9x0+j4MfwHHtxhD9FQCMH
az4WR345A/9TTMO+OX2HBEYJJOrMhc/ch8WMNMXQR7M9GxBqNftPvzIIeJArdekX
VwOI7apDcm5MjH+Y8PhKbOoJH4jLbdNtztWHmzqSA8U7qx06BpkI0lY=
-----END CERTIFICATE-----
EOF
```