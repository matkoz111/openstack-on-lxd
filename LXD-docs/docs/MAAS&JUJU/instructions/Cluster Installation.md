```
ssh-keygen -t rsa -b 4096
ssh-copy-id -h
ssh-copy-id infra2
cat /etc/ssh/sshd_config
cat /etc/ssh/sshd_config | grep -i root
ssh infra2
ssh infra3
```
# Refreshing packages
```
sudo apt update
sudo apt upgrade
```
# Ceph cluster
## Preparation of the Python environment
```
mkdir ceph
python3 -m venv ceph
apt install python3.8-venv
python3 -m venv ceph
cd ceph/
. bin/activate
pip3 install wheel
pip3 install git+https://github.com/ceph/ceph-deploy.git
```

## Installing Ceph
### Environment update
```
env DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical
apt-get --assume-yes -q update
```
### Deploying all the modules to the cluster nodes
```
ceph-deploy --username root install infra1 infra2 infra3 --release pacific --mon --mgr --mds --osd
ceph-deploy --username root mon create infra1 infra2 infra3
ceph-deploy --username root admin infra1 infra2 infra3
ceph-deploy --username root mgr create infra1 infra2 infra3
ceph-deploy --username root mds create infra1 infra2 infra3
```

### Checking the status of the cluster
```
vgdisplay 
lvdisplay 
lvcreate --help
```

### Allocating the storage volumes
```
lvcreate -l 100%FREE -n ceph-lv ubuntu-vg
lvdisplay 

lxc storage create lvm-pool lvm source=storage1-vg lvm.thinpool_name=my-pool
lxc storage create remote-lvm lvm source=storage1-vg lvm.thinpool_name=lvm-pool
```

### Creating the shared block storage space
```
ceph-deploy --username root osd create --data /dev/ubuntu-vg/ceph-lv infra1
ceph-deploy --username root osd create --data /dev/ubuntu-vg/ceph-lv infra2
ceph-deploy --username root osd create --data /dev/ubuntu-vg/ceph-lv infra3
```

### Creating the Ceph-FS space
```
ceph osd pool create cephfs_data 8
ceph osd pool create cephfs_metadata 8
ceph fs new store-cephfs cephfs_metadata cephfs_data
ceph fs set store-cephfs allow_new_snaps true
```

### Checking the status of the storage
```
ceph status
ceph osd tree
deactivate
```

# LXD cluster

## Installing LXD
```
cd ..
snap install lxd
snap refresh lxd --channel=stable
```

## Initializing LXD cluster on the first node
```
lxd init
lxc storage list
```

## Adding nodes to the cluster
```
lxc cluster add infra2
lxc cluster add infra3
```

## Try out basic container operations
```
lxc launch images:ubuntu/20.04 u1
lxc list
lxc config show u1 --expanded
lxc launch images:ubuntu/20.04 u2 --storage remote
lxc list
lxc launch images:ubuntu/20.04 u3
lxc launch images:ubuntu/20.04 u4 --storage remote
lxc list
lxc stop u1
lxc move u1 --target infra2
lxc list
lxc start u1
lxc list
```

## Create remote storage pool on Ceph-FS
```
lxc storage create remote-fs cephfs source=store-cephfs/lxd --target infra1
lxc storage create remote-fs cephfs source=store-cephfs/lxd --target infra2
lxc storage create remote-fs cephfs source=store-cephfs/lxd --target infra3
lxc storage create remote-fs cephfs
```

## Create a Ceph-FS shared storage volume
```

lxc storage volume create remote-fs shared-data
lxc profile device add infra shared-data disk pool=remote-fs source=shared-data path=/shared

lxc storage volume delete remote-fs shared-data
lxc storage delete remote-fs
lvremove ubuntu-vg/ceph-lv


lxc profile show infra
lxc profile show default
```

## Reviev of the LXD configuration
```
lxd init --dump
```

# Network configuration
```
lxc network list
apt-get install vlan
modprobe 8021q

ip link add link bond0 name VLAN10 type vlan id 10
ip link set VLAN10 up
ip addr add 10.10.10.91/24 dev VLAN10
ip link add link bond0 name VLAN999 type vlan id 0
ip link set VLAN999 up
ip addr add 10.10.99.11/24 dev VLAN999

ip route show default 0.0.0.0/0
lxc config set core.https_address "[::]:8443"
lxc config set core.trust_password infra123
```

## Customizing profile - network configuration
```
lxc network list
lxc profile show default
lxc profile copy default infra
lxc profile list

lxc profile device set infra eth0 nictype macvlan
lxc profile device set infra eth0 parent VLAN10
lxc profile device remove infra eth0
lxc profile device add infra eth0 nic nictype=macvlan parent=VLAN10
lxc profile show infra

lxc profile device remove infra eth0
lxc profile device add infra eth0 nic name=eth0 nictype=macvlan parent=VLAN10
lxc profile show infra
lxc profile device set infra root pool remote
lxc profile show infra
```
```
lxc profile list
lxc profile copy default infra
lxc profile list
lxc profile show infra
```
```
lxc networks show
lxc network list
lxc profile show infra
lxc profile set infra eth0 network bond0
lxc profile device set infra eth0 network bond0
lxc profile show infra

lxc profile device remove infra eth0
lxc profile device add infra eth0 name=eth0 nictype=ipvlan parent=bond0 type=nic
lxc profile device set infra root pool remote
lxc profile show infra
```

# MAAS cluster

## Customizing infra profile
```
lxc profile show infra

lxc storage volume create remote-fs shared-data
lxc profile device add infra shared-data disk pool=remote-fs source=shared-data path=/shared
lxc profile show infra
```

## Setting up containers for MAAS controllers
```
lxc launch images:ubuntu/20.04/cloud maas1 -p infra --target infra1
lxc launch images:ubuntu/20.04/cloud maas2 -p infra --target infra2
lxc launch images:ubuntu/20.04/cloud maas3 -p infra --target infra3
lxc list
lxc exec maas1 bash
```

# Configuring networking

## Verifying network configuration
```
modprobe --first-time 8021q
modinfo 8021q
ip a
arp -a
```


```
 10.10.10.1/24 dev bond0
ip addr del 10.10.10.1/24 dev bond0


brctl show
apt install bridge-utils
brctl show
```