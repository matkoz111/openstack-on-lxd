# Installing MAAS cluster

## Refreshing packages and installing snapd
```
snap list
apt-get update
apt-get upgrade
apt-get install snapd
```

## Configuring network
```
cat /etc/netplan/10-lxc.yaml 
vim /etc/netplan/10-lxc.yaml 
netplan apply /etc/netplan/10-lxc.yaml
ip a

ping 10.10.10.1
ping google.com
```

## Configuring and verifying connectivity to the database
```
vim /etc/host
ls /etc/host
ls /etc/
vim /etc/hosts
ping infradb1

apt-install wget
apt-get install wget
wget "postgres://maas@infradb/maas"
ping infradb1:5432
nc -vz 10.10.10.111 5432
nc -vz infradb1 5432
```

## Installing MAAS
```
snap install maas
```

## Initializing MAAS and creating the admin account
```
maas init region+rack --database-uri "postgres://maas:infra123@maasdb0/maas"
maas createadmin
```

## /var/snap/maas/current/rackd.conf
```
maas_url: http:/10.10.10.101:5240/MAAS
maas_url: http:/10.10.10.102:5240/MAAS
maas_url: http:/10.10.10.103:5240/MAAS
```
## /var/snap/maas/common/maas/secret
```
5139d9b810bf7187030fe1620281e8af
```
## Login to the API Servet
```
QVkV9rnXC7yNqtPRZr:WREPU6XKHFVB3ghkFA:ZALUrJPmWtuFpTYURVVzA4ELjmrkeTd3

maas login maasadmin http://10.10.10.101:5240/MAAS/
maas maasadmin --help

maas maasadmin machines create deployed=true hostname=juju1 architecture=amd64 mac_addresses=00:16:3e:1b:40:25 mac_addresses=00:16:3e:94:39:b4 power_type=manual


Execute as root:
wget http://10.10.10.101:5240/MAAS/maas-run-scripts && chmod 755 maas-run-scripts

./maas-run-scripts register-machine --hostname juju-controller-test http://10.10.10.104:5240/MAAS Gebpy4fTWpFKfgkEhk:ckcNf3YCd9F3TZeugN:h3X9JNL9vws9GjwLwqmdWk26HyVd82G2

./maas-run-scripts report-results --config juju-controller-test-creds.yaml

maas maasadmin machines create deployed=true hostname=juju-controller-test architecture=amd64 power_type=manual mac_addresses=00:16:3e:af:24:d8	mac_addresses=00:16:3e:90:ce:47	mac_addresses=00:16:3e:f4:b7:af

```