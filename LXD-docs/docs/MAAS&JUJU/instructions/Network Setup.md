# Automatic setup of network adapters 

The guide assumes user is inside of lxd automation ansible repo

# Check infra.ini file 
```
grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' infra.ini
```
# Run network create role(if uncertain of the outcome you can always use -c )

```
ansible-playbook -i infra.ini -vv network_create.yml
````
