## Refreshing packages and chcecking vault 
```
snap refresh maas

snap list maas 
```

## In vault
```

root@maas-vault:~# cat policy
path "secret/metadata/maas/" {
capabilities = ["list"]
}

path "secret/metadata/maas/*" {
capabilities = ["read", "update", "delete", "list"]
}

path "secret/data/maas/*" {
capabilities = ["read", "create", "update", "delete"]
}

export VAULT_TOKEN=hvs.wiAl3ytJzivy5XGMySSdJEQT

export VAULT_ADDR=http://127.0.0.1:8200

vault write -force -wrap-ttl=5m auth/approle/role/maas-ha-1/secret-id

vault read auth/approle/role/maas-ha-1/role-id
```
## Integration
```
export APPROLE_ID=b2901073-6dcf-c8ca-af27-a16bdf17aa74
export WRAPPED_TOKEN=hvs.CAESIMvMFDvavj8KPf4AElWqLKwqitJ1lSsukDW_F_fc2PcJGh4KHGh2cy42eVNEdjB1cVVCOEJNMEN4UFNJZ0tKSUY
export SECRETS_PATH=maas
export URL=http://10.10.10.107:8200
export SECRET_MOUNT=secret
maas config-vault configure $URL $APPROLE_ID $WRAPPED_TOKEN $SECRETS_PATH
```