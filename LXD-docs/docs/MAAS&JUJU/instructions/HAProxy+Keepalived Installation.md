
```
cat >> /etc/keepalived/check_apiserver.sh <<EOF
#!/bin/sh

errorExit() {
  echo "*** $@" 1>&2
  exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q 10.10.10.90; then
  curl --silent --max-time 2 --insecure https://10.10.10.90:6443/ -o /dev/null || errorExit "Error GET https://10.10.10.90:6443/"
fi
EOF
```

```
chmod +x /etc/keepalived/check_apiserver.sh
```

```
cat >> /etc/keepalived/keepalived.conf <<EOF
vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
  timeout 10
  fall 5
  rise 2
  weight -2
}

vrrp_instance VI_1 {
    state BACKUP
    interface VLAN10
    virtual_router_id 1
    priority 100
    advert_int 5
    authentication {
        auth_type PASS
        auth_pass mysecret
    }
    virtual_ipaddress {
        10.10.10.90
    }
    track_script {
        check_apiserver
    }
}
EOF
```

```
cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend infradb
  bind 10.10.10.110:5432
  mode tcp
  option tcplog
  default_backend infradb-backend

backend infradb-backend
  option httpchk GET /healthz
  http-check expect status 200
  mode tcp
  option ssl-hello-chk
  balance roundrobin
    server infradb1 10.10.10.111:5432 check fall 3 rise 2
    server infradb2 10.10.10.112:5432 check fall 3 rise 2
    server infradb3 10.10.10.113:5432 check fall 3 rise 2

EOF
```