# VIP:

| IP            | Usługa        | Host        | Type |
| ------------- | ------------- | ----------- | ---- |
| 10.10.10.90   | LXD/Ceph      | infraX      | phys |
| 10.10.10.100  | MAAS          | maasX       | cont |
| 10.10.10.110  | PostgreSQL    | infradbX    | cont |
| 10.10.10.120  | HC Vault      | vaultX      | vm   |
| 10.10.10.130  | Juju          | jujuX       | cont |
| 10.10.10.190  | ETCD          | etcdX       | cont |
| 10.10.10.200  | Charmed K8S   | k8s-masterX | cont |