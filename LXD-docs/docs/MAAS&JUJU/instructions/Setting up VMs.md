# Installing Windows VM

## Preparing the ISO image for Windows 2022 Server
```
apt-get install genisoimage
apt-get install hivexregedit
apt-get install libwin-hivex-perl
apt-get install wimtools

snap install distrobuilder --edge --classic
distrobuilder repack-windows /home/mother/SW_DVD9_Win_Server_STD_CORE_2022_2108.3_64Bit_English_DC_STD_MLF_X22-94021.iso /home/mother/win2022srv.lxd.iso --windows-version=2k22

ls /home/mother/
scp /home/mother/win2022srv.lxd.iso ubuntu@10.10.99.192:win2022srv.lxd.iso
```

## Initialize an empty VM
```
lxc init vm02-win2022 --vm --empty
lxc config set vm02-win2022 limits.cpu=4 limits.memory=8GiB
lxc config set vm02-win2022 limits.cpu=8 limits.memory=16GiB

lxc config device add vm02-win2022 vtpm tpm path=/dev/tpm0
lxc config device add vm02-win2022 vtpm tpm
lxc config device set vm02-win2022 vtpm path=/dev/tpm0
lxc config device override vm02-win2022 root size=100GiB
lxc config device add vm02-win2022 install disk source=/home/ubuntu/win2022srv.lxd.iso boot.priority=10

lxc config show vm02-win2022
lxc start vm02-win2022
lxc list
```

## Installing Remote Viewer and Windows LXD Client
```
On the server:

lxc config set core.https_address :8443
lxc config set core.trust_password some-password


On Windows:

choco install lxc

lxc remote add my-server IP-ADDRESS
[accept fingerprint and enter the password you set earlier]
lxc remote switch my-server
lxc list
lxc console NAME --type=vga
``` 

# Installing Linux VMs with cloud-init configuration

## Configuring a profile with multiple NIC
```
apt-get install --download-only virt-viewer
ls /var/cache/apt/archives
ls /var/cache/apt/archives -l
exit
apt-get install virt-viewer
ip a
lxc list
lxc start vm02-win2022 --console=vga
lxc list
lxc console vm02-win2022 --type=vga
lxc list

lxc config set core.trust_password infra123
lxc list

lxc profile copy default mcvlan
lxc profile copy default macvlan
lxc profile delete mcvlan
lxc profile edit macvlan
lxc profile show default

lxc config show vm02-win2022 --expanded
lxc profile edit macvlan
ip route show default 0.0.0.0/0
lxc profile edit macvlan
lxc profile show macvlan
lxc launch images:ubuntu/20.04 vl1 -p macvlan
```

## Adding cloud-init network configuration
```
lxc init images:ubuntu/20.04/cloud infradb1 -p infra --target infra1
vim cloud-init.infra.yaml
cat cloud-init-infra.1.yaml | sed -e 's/IP_ADDR_10/10.10.10.101/g;s/IP_GATEWAY/10.10.10.1/g' | lxc config set infradb1 user.network-config -
cat cloud-init-maas.1.yaml | sed -e 's/IP_ADDR_10/10.10.10.101/g;s/IP_GATEWAY/10.10.10.1/g;s/IP_ADDR_99/10.10.99.11/g' | lxc config set maas1 user.network-config -
lxc config show vl1
lxc start vl1

lxc exec vl1 bash
lxc profile show 2macvlans
lxc init images:ubuntu/20.04/cloud vl2 -p 2macvlans
vim vl2-network.yaml
cat vl2-network.yaml | lxc config set vl2 user.network-config -
lxc config show vl2 --expanded
lxc start vl2
lxc list
```
```
vim vl1-network.yaml
vim vl2-network.yaml
lxc config show vl2
```

cat /home/ubuntu/cloud-init-scripts/cloud-init-infra.1.yaml | sed -e 's/IP_ADDR_10/10.10.10.201/g;s/IP_GATEWAY/10.10.10.1/g' | lxc config set rnd-demo1 user.network-config -