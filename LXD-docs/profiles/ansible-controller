config:
  boot.autostart: "true"
  linux.kernel_modules: ip_tables,ip6_tables,netlink_diag,nf_nat,overlay
  raw.lxc: |
    lxc.mount.auto=proc:rw sys:rw
  security.nesting: "true"
  security.privileged: "true"
  limits.cpu: "2"
  limits.memory: 4GB
  user.user-data: |
    #cloud-config
    packages:
      - snapd
      - ssh
      - net-tools
      - jq
      - nano
      - traceroute
      - screen
      - iptables
      - rsync
      - git
      - python3-pip
    users:
      - default
      - name: mother
        passwd: Eiz(ae#ko3mee@gh
        groups: sudo
        sudo: ALL=(ALL) NOPASSWD:ALL
        ssh_authorized_keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLYl/2jfGlX4oC9caSBTTuPBY0iMI24kK/y6h081CIMmpM8wRmbxPWlhLl+H8o961trhFtoqb9VCxwsztUwIjtGhjMXlLTTdeZ/50O+crOclFnr2kyEyLSKIN1puR1yk/qGOGVpc+F0qh3iUIovZ5V0KP1wDX0Cl/bjAIVsncrukcZgud9FqWPVhebMS2LWehD12jm1m1sdtKBhTS6vANP7mSPXXoPU9xV1DWIgSmbas2qykAQbcQLklxE4bS2AR1uYvtRe2BkSAuzR31fCZiausWNoHWiqhgnd1qJzhc1tI/roDvEqyyJ/F3Tc8NWUg2MOJgdejHgyJ3/9AZCP6A+NN+wIkJeMyhs61C09BOOkhKzLNjB9gbM1Ixpu7YTqMXwUvxg1WRY33M1XoBrIAGpH3ncaw8aTV6vDZUSJOfSWypz4c/MRSfH4qkTwYXavNP2XnMSL9IKI8DB8wMtjGgtEv+hGF/akVW7Z39GhwX0xEMA0f6vrQ1YPjkVz2/1OCfdQMlWULyb9IFmiDeL7nLvL/GV1xze7F3yvwU+MV3x5LLtczo2TKopBE3DlDnd1UYXoHjjLVqxCtuQYvOJhVsNj1ug7m4rkVVzGhn+6nCDnCtTB8zDMvPdm7iWH/J2Szk6xp83FW4zKeyEysnqfP4oeVb3cfT1t87mjCLm/v1shw== admin@netrix.com.pl
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDQKP5UbCSwADFNJsWF9z1zMsEQM6Kh8JOOug4ZYq2JsMYGSqZ0HgeLJzJrfI6OQKUZnyE06wz15UgIS/mlFf8xt2CDIFYc86RTTin8De+2nFEvWgi+sdG4TXqvvMpTfJQTORaGkjuXglG/9gxsKdw1PGi07Bmd9nis0I0Cry+3x7/ULS7UCHi7jnOfegaANQ23EIVmWVpsq2xEabd964IJLZkV7Vnx3HUPsjXyfIn+gZ27QuXfnAfmOYmQ64JPVbFKx573eXAsFe7VQNrPcw73GG4sVH+5Uo2a3Vvw/orBjTkNjQr8fdJ5FSi8o57jeschwOdIVQhiOfv2zU96WdoGtLYDksbd+j3Do69i2ugRr3wjNlAe4n65syUrmwR0QCZgL5SgCt7Fz6uN/wsLsVHexWauPXVEn+nbBjImNKo/LjMC9sb8BxAwpkNfgEz3eXBzUuie0/67WNuxUYbi1w8QPDE0FpsI/610Hpsd4tuV4TWzjouNuXvNSE2CFD+WjP1dRS9wn/tupQUG0/PSat5XEpZtbFumdXDjf+9qATRr7t7aEXgwmZXuJAxtBOXdcZFvA/Iyo34UxzNbnTheqLX3qICqAbusOV0jNMQCMQUzrhyq6XH0RrP6O5a+AmdUYurcqEXg0h/nO90FH7L19S7548vD1O0RkM8chwLE9l+pXw== mother@infra1
      - name: ansible
        gecos: Ansible User
        shell: /bin/bash
        groups: users,admin,wheel,lxd
        sudo: ALL=(ALL) NOPASSWD:ALL
    lxd:
      init:
        storage_backend: dir
    ansible:
      install_method: pip
      package_name: ansible
      run_user: ansible
      galaxy:
        actions:
          - ["ansible-galaxy", "collection", "install", "community.general"]

      setup_controller:
        repositories:
          - path: /home/ansible/my-repo/
            source: git@github.com:holmanb/ansible-lxd-private.git
        run_ansible:
          - playbook_dir: /home/ansible/my-repo
            playbook_name: start-lxd.yml
            timeout: 120
            forks: 1
            private_key: /home/ansible/.ssh/id_rsa2
          - playbook_dir: /home/ansible/my-repo
            playbook_name: configure-lxd.yml
            become_user: ansible
            timeout: 120
            forks: 1
            private_key: /home/ansible/.ssh/id_rsa2
            inventory: new_ansible_hosts

    write_files:
      - path: /home/ansible/.ssh/known_hosts
        owner: ansible:ansible
        permissions: 0o600
        defer: true
        content: |
          |1|YJEFAk6JjnXpUjUSLFiBQS55W9E=|OLNePOn3eBa1PWhBBmt5kXsbGM4= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl
          |1|PGGnpCpqi0aakERS4BWnYxMkMwM=|Td0piZoS4ZVC0OzeuRwKcH1MusM= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
          |1|OJ89KrsNcFTOvoCP/fPGKpyUYFo=|cu7mNzF+QB/5kR0spiYmUJL7DAI= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEmKSENjQEezOmxkZMy7opKgwFB9nkt5YRrYMjNuG5N87uRgg6CLrbo5wAdT/y6v0mKV0U2w0WZ2YB/++Tpockg=

      - path: /home/ansible/.ssh/id_rsa2
        owner: ansible:ansible
        permissions: 0o600
        defer: true
        encoding: base64
        content: |
          LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFB
          QUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFB
          d0VBQVFBQUFZRUEwUWlRa05WQS9VTEpWZzBzT1Q4TEwyMnRGckg5YVR1SWFNT1FiVFdtWjlNUzJh
          VTZ0cDZoClJDYklWSkhmOHdsaGV3MXNvWmphWVVQSFBsUHNISm5UVlhJTnFTTlpEOGF0Rldjd1gy
          ZTNBOElZNEhpN0NMMDE3MVBoMVUKYmJGNGVIT1JaVkY2VVkzLzhmbXQ3NmhVYnpiRVhkUXhQdVdh
          a0IyemxXNTdFclpOejJhYVdnY2pJUGdHV1RNZWVqbEpOcQpXUW9MNlFzSStpeUlzYXNMc1RTajha
          aVgrT1VjanJEMUY4QXNKS3ZWQStKbnVZNUxFeno1TGQ2SGxGc05XVWtoZkJmOWVOClpxRnJCc1Vw
          M2VUY1FtejFGaHFFWDJIQjNQT3VSTzlKemVGcTJaRE8wUlNQN09acjBMYm8vSFVTK3V5VkJNTDNi
          eEF6dEIKQWM5dFJWZjRqcTJuRjNkcUpwVTFFaXZzR0sxaHJZc0VNQklLK0srVzRwc1F5c3ZTL0ZK
          V2lXZmpqWVMwei9IbkV4MkpHbApOUXUrYkMxL1dXSGVXTGFvNGpSckRSZnNIVnVscTE2MElsbnNx
          eGl1MmNHd081V29Fc1NHdThucXB5ZzQzWkhDYjBGd21CCml6UFFEQVNsbmlXanFjS21mblRycHpB
          eTNlVldhd3dsTnBhUWtpZFRBQUFGZ0dLU2o4ZGlrby9IQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5F
          SWtKRFZRUDFDeVZZTkxEay9DeTl0clJheC9XazdpR2pEa0cwMXBtZlRFdG1sT3JhZW9VUW15RlNS
          My9NSgpZWHNOYktHWTJtRkR4ejVUN0J5WjAxVnlEYWtqV1EvR3JSVm5NRjludHdQQ0dPQjR1d2k5
          TmU5VDRkVkcyeGVIaHprV1ZSCmVsR04vL0g1cmUrb1ZHODJ4RjNVTVQ3bG1wQWRzNVZ1ZXhLMlRj
          OW1tbG9ISXlENEJsa3pIbm81U1RhbGtLQytrTENQb3MKaUxHckM3RTBvL0dZbC9qbEhJNnc5UmZB
          TENTcjFRUGlaN21PU3hNOCtTM2VoNVJiRFZsSklYd1gvWGpXYWhhd2JGS2QzawozRUpzOVJZYWhG
          OWh3ZHp6cmtUdlNjM2hhdG1RenRFVWorem1hOUMyNlB4MUV2cnNsUVRDOTI4UU03UVFIUGJVVlgr
          STZ0CnB4ZDNhaWFWTlJJcjdCaXRZYTJMQkRBU0N2aXZsdUtiRU1yTDB2eFNWb2xuNDQyRXRNL3g1
          eE1kaVJwVFVMdm13dGYxbGgKM2xpMnFPSTBhdzBYN0IxYnBhdGV0Q0paN0tzWXJ0bkJzRHVWcUJM
          RWhydko2cWNvT04yUndtOUJjSmdZc3owQXdFcFo0bApvNm5DcG41MDY2Y3dNdDNsVm1zTUpUYVdr
          SkluVXdBQUFBTUJBQUVBQUFHQUV1ejc3SHU5RUVaeXVqTE9kVG5BVzlhZlJ2ClhET1pBNnBTN3lX
          RXVmanc1Q1NsTUx3aXNSODN5d3cwOXQxUVd5dmhScUV5WW12T0JlY3NYZ2FTVXRuWWZmdFd6NDRh
          cHkKL2dRWXZNVkVMR0thSkFDL3E3dmpNcEd5cnhVUGt5TE1oY2tBTFUyS1lnVisvcmovajZwQk1l
          VmxjaG1rM3Bpa1lyZmZVWApKRFk5OTBXVk8xOTREbTBidUxSekp2Zk1LWUYyQmNmRjRUdmFyak9Y
          V0F4U3VSOHd3dzA1MG9KOEhkS2FoVzdDbTVTMHBvCkZSbk5YRkdNbkxBNjJ2TjAwdkpXOFY3ajd2
          dWk5dWtCYmhqUldhSnVZNXJkRy9VWW16QWU0d3ZkSUVucGs5eEluNkpHQ3AKRlJZVFJuN2xUaDUr
          L1FsUTZGWFJQOElyMXZYWkZuaEt6bDBLOFZxaDJzZjRNNzlNc0lVR0FxR3hnOXhkaGpJYTVkbWdw
          OApOMThJRURvTkVWS1ViS3VLZS9aNXlmOFo5dG1leGZIMVl0dGptWE1Pb2pCdlVISWpSUzVoZEk5
          TnhuUEdSTFkya2pBemNtCmdWOVJ2M3Z0ZEYvK3phbGszZkFWTGVLOGhYSytkaS83WFR2WXBmSjJF
          WkJXaU5yVGVhZ2ZOTkdpWXlkc1F5M3pqWkFBQUEKd0JOUmFrN1VycW5JSE1abjdwa0NUZ2NlYjFN
          ZkJ5YUZ0bE56ZCtPYmFoNTRIWUlRajVXZFpUQkFJVFJlTVpOdDlTNU5BUgpNOHNRQjhVb1pQYVZT
          QzNwcElMSU9mTGhzNktZajZSckdkaVl3eUloTVBKNWtSV0Y4eEdDTFVYNUNqd0gyRU9xN1hoSVd0
          Ck13RUZ0ZC9nRjJEdTdIVU5GUHNaR256SjNlN3BES0RuRTd3MmtoWjhDSXBURmdENzY5dUJZR0F0
          azQ1UVlURG81SnJvVk0KWlBEcTA4R2IvUmhJZ0pMbUlwTXd5cmVWcExMTGU4U3dvTUpKK3JpaG1u
          Slp4TzhnQUFBTUVBMGxoaUtlemVUc2hodDR4dQpyV2MwTnh4RDg0YTI5Z1NHZlRwaERQT3JsS1NF
          WWJrU1hoanFDc0FaSGQ4UzhrTXIzaUY2cG9PazNJV1N2Rko2bWJkM2llCnFkUlRnWEg5VGh3azRL
          Z3BqVWhOc1F1WVJIQmJJNTlNbytCeFNJMUIxcXptSlNHZG1DQkw1NHd3elptRktEUVBRS1B4aUwK
          bjBNbGM3R29vaURNalQxdGJ1Vy9PMUVMNUVxVFJxd2dXUFRLaEJBNnI0UG5HRjE1MGhaUklNb29a
          a0Qyelg2YjFzR29qawpRcHZLa0V5a1R3bktDekY1VFhPOCt3SjNxYmNFbzlBQUFBd1FEK1owcjY4
          YzJZTU5wc215ajNaS3RaTlBTdkpOY0xteUQvCmxXb05KcTNkakpONHMySmJLOGw1QVJVZFczeFNG
          RURJOXl4L3dwZnNYb2FxV255Z1AzUG9GdzJDTTRpMEVpSml5dnJMRlUKcjNKTGZEVUZSeTNFSjI0
          UnNxYmlnbUVzZ1FPelRsM3hmemVGUGZ4Rm9PaG9rU3ZURzg4UFFqaTFBWUh6NWtBN3A2WmZhegpP
          azExckpZSWU3K2U5QjBsaGt1MEFGd0d5cWxXUW1TL01oSXBuakhJazV0UDRoZUhHU216S1FXSkRi
          VHNrTldkNmFxMUc3CjZIV2ZEcFg0SGdvTThBQUFBTGFHOXNiV0Z1WWtCaGNtTT0KLS0tLS1FTkQg
          T1BFTlNTSCBQUklWQVRFIEtFWS0tLS0tCg==

      - path: /root/.ssh/id_rsa
        owner: root:root
        permissions: 0o600
        defer: true
        encoding: base64
        content: |
          LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFB
          QUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUNGd0FBQUFkemMyZ3RjbgpOaEFBQUFB
          d0VBQVFBQUFnRUEwQ2orVkd3a3NBQXhUU2JGaGZjOWN6TEJFRE9pb2ZDVGpyb09HV0t0aWJER0Jr
          cW1kQjRICml5Y3lhM3lPamtDbEdaOGhOT3NNOWVWSUNFdjVwUlgvTWJkZ2d5QldIUE9rVTA0cC9B
          M3Z0cHhSTDFvSXZySFJ1RTE2cjcKektVM3lVRXprV2hwSTdsNEpSdi9ZTWJDbmNOVHhvdE93Wm5m
          WjRyTkNOQXE4dnQ4ZS8xQzB1MUFoNHU0NXpuM29HZ0RVTgp0eENGWmxsYWJLdHNSR20zZmV1Q0NT
          MlpGZTFaOGR4MUQ3STE4bnlKL29HZHUwTGwzNXdINWptSmtPdUNUMVd4U3NlZTkzCmx3TEJYdTFV
          RGF6M01POXhodUxGUi91VktObXQxYjhQNkt3WTA1RFkwSy9IM1NlUlVvdktPZTQzckhJY0RuU0ZV
          SVlqbjcKOXMxUGVsbmFCclMyQTVMRzNmbzl3Nk92WXRyb0VhOThJelpRSHVKK3ViTWxLNXNFZEVB
          bVlDK1VvQXJleGMrcmpmOExDNwpGUjNzVm1yajExUkovcDJ3WXlKalNxUHk0ekF2YkcvQWNRTUta
          RFg0Qk05M2x3YzFMb250UCt1MWpic1ZHRzR0Y1BFRHd4Ck5CYWJDUCt0ZEI2YkhlTGJsZUUxczQ2
          TGpibDd6VWhOZ2hRL2xvejlYVVV2Y0ovN2JxVUZCdFB6MG1yZVZ4S1diV3hicG4KVnc0My92YWdF
          MGErN2UyaEY0TUptVjdpUU1iUVRsM1hHUmJ3UHlNcU4rRk1jelc1MDRYcWkxOTZpQXFnRzdyRGxk
          SXpURQpBakVGTTY0Y3F1bHg5RWF6K2p1V3ZnSm5WR0xxM0toRjROSWY1enZkQlIreTlmVXUrZVBM
          dzlUdEVaRFBISWNDeFBaZnFWCjhBQUFkSThQeDlPL0Q4ZlRzQUFBQUhjM05vTFhKellRQUFBZ0VB
          MENqK1ZHd2tzQUF4VFNiRmhmYzljekxCRURPaW9mQ1QKanJvT0dXS3RpYkRHQmtxbWRCNEhpeWN5
          YTN5T2prQ2xHWjhoTk9zTTllVklDRXY1cFJYL01iZGdneUJXSFBPa1UwNHAvQQozdnRweFJMMW9J
          dnJIUnVFMTZyN3pLVTN5VUV6a1docEk3bDRKUnYvWU1iQ25jTlR4b3RPd1puZlo0ck5DTkFxOHZ0
          OGUvCjFDMHUxQWg0dTQ1em4zb0dnRFVOdHhDRlpsbGFiS3RzUkdtM2ZldUNDUzJaRmUxWjhkeDFE
          N0kxOG55Si9vR2R1MExsMzUKd0g1am1Ka091Q1QxV3hTc2VlOTNsd0xCWHUxVURhejNNTzl4aHVM
          RlIvdVZLTm10MWI4UDZLd1kwNURZMEsvSDNTZVJVbwp2S09lNDNySEljRG5TRlVJWWpuNzlzMVBl
          bG5hQnJTMkE1TEczZm85dzZPdll0cm9FYTk4SXpaUUh1Sit1Yk1sSzVzRWRFCkFtWUMrVW9BcmV4
          YytyamY4TEM3RlIzc1ZtcmoxMVJKL3Ayd1l5SmpTcVB5NHpBdmJHL0FjUU1LWkRYNEJNOTNsd2Mx
          TG8KbnRQK3UxamJzVkdHNHRjUEVEd3hOQmFiQ1ArdGRCNmJIZUxibGVFMXM0NkxqYmw3elVoTmdo
          US9sb3o5WFVVdmNKLzdicQpVRkJ0UHowbXJlVnhLV2JXeGJwblZ3NDMvdmFnRTBhKzdlMmhGNE1K
          bVY3aVFNYlFUbDNYR1Jid1B5TXFOK0ZNY3pXNTA0ClhxaTE5NmlBcWdHN3JEbGRJelRFQWpFRk02
          NGNxdWx4OUVheitqdVd2Z0puVkdMcTNLaEY0TklmNXp2ZEJSK3k5ZlV1K2UKUEx3OVR0RVpEUEhJ
          Y0N4UFpmcVY4QUFBQURBUUFCQUFBQ0FEb3Fpazc2M1JHVkxER2IyT3dEVjFkK1lqWnR4MDhyRTRC
          NQpld0VhOHRVYjRXbldxRWFnTVlsdnpZWDlpelF5MmkrYnFNTEp2NWhpbGt0RE42SXEwR0dMRlZ2
          dXlFRlV6NkU3b1QyK2d6CkpxNGNDeWp1a0NkVkRqelptWHpvMmZyUnptQWVaZXJHbzh6eTB0eXZE
          YWpaOE9uMCtXL2QySEJ0ZDdlenU1eWtLM2ZXSUQKc2ljcnF0ajJDRmNTMlFnd1N3SFJpd1U3aUtu
          L0NNVGhLWlgxSGlRRWF0ZVd1R0IrTXZQTTM0Zy94cXVvZW5QZVVLKzRRUApqVE1SNmV3STM4cXdy
          elZ2ZGVKcnNhOGQ4RFBXRmtqRlQ5cmFPVGhSRTdBVGk2RER2K1ZMV2JDY3RBazBaVXZtQjVTaVhV
          CkxzenYvUDd3a1hCRDJwRGdGUWswKzdWN3NCRllWdUxtME5jZFJ6dXlBNkoveHBxT0xPdVh5dktX
          Y0I2cGxiZ1VvRFFmYXcKK3BKMDFlTHh4ZGF2a1lUMjJubHJSWks3V3NDeGlaZVdDS3dxOWx6elpR
          WElVdU45NmVRbDI4RzN0NGc3eHJTNXhyaTBhbQpvVTBUbmVvOHVDS01KejBoK2tpLytSalJ3Z09M
          ZDNtUDlIbHhXVDF2Uk9FbEJ6Z3ovT2syUHB1ZWdqTmJLbHl5UmtHdEdBCjV2N1I3bmpxOWZXQnJU
          NWRZNEp4dGlvSEd1aFl2QktiVzNtZkRwKzZKazZPWnZ2cTlnVkpOTzRQOXhPeHlxNCtvd29Jc3EK
          TXp3bGtud3QxT00rOFNRR3FEeHJEV1Y3VUt1Z2pOTmpHREF6MHhaT3ptaFFiUE9nbVVZRjk0WFY3
          dHlraUpRYWNtWHNUYQo2bG1RQ1BERnBPQjNLU3BZMHhBQUFCQUREeTJ6SS9QVnZCVjMrNlpJcWZl
          YkF0YzVJak8vWnFkdzNyRVRuUC9MQWxHZWRyCjN6ZXUwZ1ZOZ0VNSDhvbnA2RmRxMU9kSjIwa3hu
          N0s1MEM2dHdDTXZnRm9icWtkUTJqSXpzQUU3Szd5aEdMNStOS1BOQngKY2pVcUVIMlV1aVJjbXRh
          M0p4Znc4ZEFLMkcyTUs4L2krM21TcW1IaTYrRjUvT0RHZnBJa2w3TkZLdW1MTHp0ZS9NYWlIZQor
          UDNMQWluVWFCNUhiRDhUZXR6SGsvcGh5TUpsRFpmbC84ckRIakkydlF0K1hRRHFyeFJUT3d0b000
          WlVJMk56T1hjTXFlCnc3c3lHcklGdTlIOFFZUGpUeUpSR2tiRjZFcHVISmh3QVEvNitBVXpVdVY2
          NmdQNUN2eHNUZjdxNWhjc0VRNTgxa0ZzeXMKVU1pN2NnZUhLYkNiWld3QUFBRUJBUGJUVXVySjFU
          SnpQR3NSd3pLN0VWbkhYNE85bTZKZEhVMDlVaWxaVDBsbmdlamUzaQp6Mm1FMERFU0FSUm8zWENx
          dFZ6UXFYVDd1UUJkbmFsMVFZdmhxRkNucEQvU0tGNTB6OTJJWkVDbmhZdUpUcnMvcWNEWkszClBZ
          LzEzTld6REdYRXhsRGpnMWRlSEVvYm5sdDd1R1VFUlJIenZ2UVRqZ1FvRjlrczRZejdwR09JWEcr
          VzFKTFVFZm1BL3QKU3VoRnE1eDFmbzUyK3JIYzVtQlQ2K2Z6anhCYUVOSmtkdmxSTHB0eGMrVHo2
          Y2VUZlpWcGtScUtCcFFnWXh4ZEJwRDJLNApZTnRxVXFLbGZiUDBJWThzTklEdFo3QnNwWG01RW5D
          a1FBK1pMZHV2cW1kV2JrbnVmc1BtRjFlbHhkSGJLT0N4eno1bmxJUFI1S3kyMlZVYTVza0FBQUVC
          QU5mbHY0VXJoQk1sMXg5WnpiMk1aNEVCd0RUdktUOVdyS0hKZHFKM09yaCtiL01vUFRuWApUNjZY
          QWJYM3NBczRrWHFlZWdJVlExbFVKSm5McHhVSm5IdGhFRmFrT2ZQcTNsVHZYYUJlcVNtK1hXTWtT
          enlWQlpQLzFECkI0eDZlQkoxQkpzY2I2SmtzMkVJZWN6RXZXT0pOZi9ETTU0RVk0UzZLRWlrdjJY
          NHM5MGdaYUVkaDZrK2EvY0R2RGRlY28KNC9RK3dJQWJOUWpiZlFGMU5CSWNVSU1HUkYyaWkxWGY0
          MFZtUkZwUUMxcWs4dGRmdmthalRsVi81Y2FTa3JZdmk5aGZibwpMeFQrcms5QTRHa0JTVHVRRllD
          bUZtYjQ0UTRHSTJJcHc0OHc4ejN0aXVKL2JDcDNQR2FjVlp0d1c1LzdGUW9PZXBTWEJsCllzTnFp
          bEhoR3VjQUFBQU5iVzkwYUdWeVFHbHVabkpoTXdFQ0F3UUZCZz09Ci0tLS0tRU5EIE9QRU5TU0gg
          UFJJVkFURSBLRVktLS0tLQo=
devices:
  eth1:
    name: eth1
    nictype: macvlan
    parent: VLAN10
    type: nic
  eth2:
    name: eth2
    network: lxdfan0
    type: nic
  root:
    path: /
    pool: remote
    size: 60GB
    type: disk